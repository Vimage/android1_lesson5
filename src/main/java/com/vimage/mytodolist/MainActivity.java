package com.vimage.mytodolist;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    ArrayList<TaskModel> tasks = new ArrayList<>();
    AdapterTasks tasksAdapter;
    CharSequence[] nChooseList = {"Perform task", "Edit task", "Delete task"};
    int selectedTaskPosition;
    TaskModel currentTask;

    // дальше идёт жуткий спагетти код :))))))))))))))

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.rvTasks);
        //fillArrayList();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        tasksAdapter = new AdapterTasks(tasks, MainActivity.this);
        recyclerView.setAdapter(tasksAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemToTaskList();
//                Snackbar.make(view, "Task has been added to list", Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            }
        });


    }


    public void onTaskListItemClicked(View view) {
        int itemAdapterPosition = recyclerView.getChildAdapterPosition(view);
        if (itemAdapterPosition == RecyclerView.NO_POSITION) {
            return;
        }
        selectedTaskPosition = itemAdapterPosition;
        currentTask = tasks.get(selectedTaskPosition);
        tasksAdapter.setFocused(itemAdapterPosition);

    }

    public void onTaskListItemLongClicked(View view) {
        int itemAdapterPosition = recyclerView.getChildAdapterPosition(view);
        if (itemAdapterPosition == RecyclerView.NO_POSITION) {
            return;
        }
        selectedTaskPosition = itemAdapterPosition;
        currentTask = tasks.get(selectedTaskPosition);
        createListDialog(view);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addItemToTaskList() {


        createCustomCodeDialog(null);


    }

    public void createTask(String description, long dueToDate) {
        currentTask = new TaskModel();
        currentTask.description = description;
        ////currentTask.description = "Новая задача";
        currentTask.dueToDate = dueToDate;
        currentTask.performDate = System.currentTimeMillis();
        tasksAdapter.addItemAtLastPosition(currentTask);
        //createCustomCodeDialog(null);


    }

    public void createCustomCodeDialog(View view) {

        //final View viewForDialog = createViewForDialog(currButton.getText().toString());
        final View viewForDialog = createViewForDialog("");
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Enter task properties")
                .setView(viewForDialog)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Create task", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((LinearLayout) viewForDialog).getChildAt(0);//(EditText) viewForDialog.findViewById(123);
                        EditText dateText = (EditText) ((LinearLayout) viewForDialog).getChildAt(1);
                        //dateText.


                        Date date = null;
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            date = format.parse(String.valueOf(dateText.getText()));

                            //System.out.println(date);

                        } catch (ParseException e) {

                            e.printStackTrace();
                        }
                        //Toast.makeText(MainActivity.this, date.toString(), Toast.LENGTH_SHORT).show();
                        createTask(editText.getText().toString(), date.getTime());

                    }
                })
                .setCancelable(false)
                .show();
    }


    public void createCustomCodeDialogForEdit(View view) {

        //final View viewForDialog = createViewForDialog(currButton.getText().toString());
        final View viewForDialog = createViewForDialog(currentTask.description);
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Enter task properties")
                .setView(viewForDialog)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Apply changes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((LinearLayout) viewForDialog).getChildAt(0);//(EditText) viewForDialog.findViewById(123);
                        EditText dateText = (EditText) ((LinearLayout) viewForDialog).getChildAt(1);
                        //dateText.


                        Date date = null;
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            date = format.parse(String.valueOf(dateText.getText()));

                            //System.out.println(date);

                        } catch (ParseException e) {

                            e.printStackTrace();
                        }
                        //Toast.makeText(MainActivity.this, date.toString(), Toast.LENGTH_SHORT).show();
                        editTask(editText.getText().toString(), date.getTime());

                    }
                })
                .setCancelable(false)
                .show();
    }

    public void editTask(String description, long dueToDate) {
        //currentTask = new TaskModel();
        currentTask.description = description;
        ////currentTask.description = "Новая задача";
        currentTask.dueToDate = dueToDate;
        currentTask.performDate = System.currentTimeMillis();
        tasksAdapter.changeItemAtPosition(selectedTaskPosition);
        //createCustomCodeDialog(null);


    }


    private View createViewForDialog(String editingText) {
        LinearLayout linearLayout = new LinearLayout(MainActivity.this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParamsForView = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParamsForView.gravity = Gravity.CENTER_HORIZONTAL;

        EditText editText = new EditText(MainActivity.this);
        if (editingText.equals("")) {
            editText.setHint("Enter task description");
        } else {
            editText.setText(editingText);
        }
        editText.setTextSize(20);
        linearLayout.addView(editText, layoutParamsForView);


// получим текущую дату
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        final EditText editDate = new EditText(MainActivity.this);
        editDate.setText(day + "-" + (month) + "-" + year);
        editDate.setInputType(InputType.TYPE_NULL);
        //editDate.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDatePickerDialog(null, editDate);


            }
        });

        linearLayout.addView(editDate, layoutParamsForView);


        return linearLayout;
    }

    DialogInterface.OnClickListener dialogIntfListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            switch (i) {
                case 0:
                    tasksAdapter.setPerormed(selectedTaskPosition);
                    //createCustomCodeDialog("fggggggggggggg");
                    //Toast.makeText(MainActivity.this, "0", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    createCustomCodeDialogForEdit(null);
                    //createCustomCodeDialog(null);
                    //deleteButton(currButton);
                    break;

                case 2:
                    tasksAdapter.removeItemAtPosition(selectedTaskPosition);
                    ////deleteButton(currButton);
                    break;
                default:


                    //Toast.makeText(MainActivity.this, nChooseList[i], Toast.LENGTH_SHORT).show();
            }
        }
    };

    public void createListDialog(View view) {


        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Choose action")
                .setItems(nChooseList, dialogIntfListener)
                .setNeutralButton("Cancel", null)
                .show();

    }

    public void createDatePickerDialog(View view, final EditText editDate) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        //String selDate = "";

        new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                //Toast.makeText(MainActivity.this, dayOfMonth + "-" +monthOfYear + "-" + year, Toast.LENGTH_SHORT).show();
                editDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
            }
        }, year, month, day).
                show();


    }


}
