package com.vimage.mytodolist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Dimon on 09.05.2016.
 */
public class TaskHolder extends RecyclerView.ViewHolder {
    ImageView ivIcon;
    TextView tvTaskDescription;
    TextView tvDueToDate;
    TextView tvPerformDate;
    TextView tvPerformDateLabel;

    RelativeLayout rlBg;

    public TaskHolder(View itemView) {
        super(itemView);
        ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);
        tvTaskDescription = (TextView) itemView.findViewById(R.id.tvTaskDescription);
        tvDueToDate = (TextView) itemView.findViewById(R.id.tvDueToDate);
        tvPerformDate = (TextView) itemView.findViewById(R.id.tvPerformDate);
        tvPerformDateLabel = (TextView) itemView.findViewById(R.id.textView2);
        rlBg = (RelativeLayout) itemView.findViewById(R.id.rl);

    }
}
